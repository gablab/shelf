const fs = require('fs');
const readline = require('readline');
const path = require('path');


separate_header_body = (filePath) => {
  return new Promise((resolve, reject) => {
    const rl = readline.createInterface({
      input: fs.createReadStream(filePath),
      crlfDelay: Infinity
    });

    var header = ''
    var body = ''
    var is_header = false

    rl.on('line', (line) => {
      let skip_line = false //line == '' ? true : false
      if (line.slice(0, 3) === '---')
      {
        is_header = !is_header
        skip_line = true
      }
      if (is_header && !skip_line) {
        //header.push(line)
        header = header.concat(line + '\n')
      }
      else if (!skip_line) {
        body = body.concat(line + '\n')
      }
    });

    rl.on('close', () => {
      resolve({header: header.trim(), body: body.trim()})
    })
  })/*.catch(() => {
    console.log('problem with file: '. filePath);
  })*/
}

header_to_properties = (header) => {
  properties = require('js-yaml').load(header)
  return properties
}

// the question mark (?) in the regexp makes it non-greedy
const re_tags = new RegExp('@t{.*?}', 'g');
const re_refs = new RegExp('@r{.*?}', 'g');
const re_label = new RegExp('@l{.*?}', 'g');
const re_spaces = new RegExp('  +' ,'g');
const re_endline = new RegExp(' *\n*$', 'g');


body_to_notes = (body) => {
  return body.split('\n\n').map(entry => {
    let tg = entry.match(re_tags);
    let rf = entry.match(re_refs);
    let label = entry.match(re_label);
    if (label) {
      if (label.length > 1) {
        console.error('error: labels should be unique');
      }
      label = label[0];
    }
    let tx = entry
      .replaceAll(re_tags, '')
      .replaceAll(re_refs, '')
      .replaceAll(re_label, '')
      .replaceAll(re_spaces, ' ')
      .replaceAll(re_endline, '');
    return {
      text: tx,
      tags: tg ? tg.map(x => x.slice(3, -1)) : [],
      refs: rf ? rf.map(x => x.slice(3, -1)) : [],
      label: label ? label.slice(3, -1) : undefined
    };
  })


}

// add to String the .hashCode() method
// https://werxltd.com/wp/2010/05/13/javascript-implementation-of-javas-string-hashcode-method/
String.prototype.hashCode = function(){
	var hash = 0;
	if (this.length == 0) return hash;
	for (i = 0; i < this.length; i++) {
		char = this.charCodeAt(i);
		hash = ((hash<<5)-hash)+char;
		hash = hash & hash; // Convert to 32bit integer
	}
	return hash;
}


create_entry = (parsed_header, note) => {
  // any better idea for deep copy? https://medium.com/aubergine-solutions/demystifying-copy-in-javascript-deep-copy-and-shallow-copy-ce374c827685. Others don't go deep enough (array by reference).
  if (parsed_header === undefined) {
    var entry = {};
  } else {
    var entry = JSON.parse(JSON.stringify(parsed_header)) 
  }

  if (note['tags'])
    entry['tags'] ? entry['tags'].push(...note['tags']) : entry['tags'] = note['tags'];
  entry['text'] = note['text'];
  entry['refs'] = note['refs'];
  entry['label'] = note['label'];
  //entry['id'] = JSON.stringify(entry).hashCode()
  return entry
}

file_to_entries = (filePath) => {
  //return new Promise((resolve, reject) => {
  ret = separate_header_body(filePath).then((separated) => {
    try {
      var parsed_header = header_to_properties(separated['header'])
      var notes = body_to_notes(separated['body'])
    entries = notes.map(
        v => {
            return create_entry(parsed_header, v);
        }
      )
    } catch (error) {
      console.log('problem with file ', filePath);
      console.log(separated['body']);
      console.error(error);
    }
    return entries
  })

  return ret 
}


read_dir = (dir, archive) => {
  return new Promise((resolve, reject) => {

    fs.readdir(dir, (err, files) => {
      console.log(dir)
      for (f of files)
      {
        filePath = path.join(dir, f);
        const parts = path.parse(filePath)
        if (['.txt', '.md'].indexOf(parts['ext']) >= 0)
        {
          file_to_entries(filePath)
            .then((entries) => {
              archive.push(...entries)
            })
        }
      }
      resolve(dir)
    })

  })
}

create_reference_archive = (archive) => {
  let ref_archive = {};
  for (entry of archive) {
    if (entry.label) {
      ref_archive[entry.label] = entry;
    }
  }
  return ref_archive;
}

enrich_archive = (archive) => {
  let ref_archive = create_reference_archive(archive);
  let enriched_archive = archive.map(entry => {
    enriched_entry = {...entry};  // copy

    enriched_entry.resolved_references = [];
    if (enriched_entry.refs)
      for (ref of enriched_entry.refs) {
        enriched_entry.resolved_references.push(ref_archive[ref]);
      }

    return enriched_entry;
  });
  return enriched_archive;
}

//export const enrich_archive = enrich_archive;
  


module.exports = {
  read_dir: read_dir,
  separate_header_body: separate_header_body,
  header_to_properties: header_to_properties,
  body_to_notes: body_to_notes,
  create_entry: create_entry,
  file_to_entries: file_to_entries,
  enrich_archive: enrich_archive
}
