

// check if there is a match between the words in two strings
var words_match = (keyw, sentence) => {
  if (keyw == undefined) return false
  if (sentence == undefined) return false
  l1 = keyw.replace(/\s*\W\s*/ig, ' ').split(' ') // \W is non-word, \s is space
  l2 = sentence.replace(/\s*\W\s*/ig, ' ')
  //l2 = sentence.replace(',',' ').replace('  ', ' ').split(' ')
  for (k of l1) {
    if (l2.match(new RegExp(k, 'i'))) return true;
  }
  return false;
}


var results_of_query = (query, archive) => {
  let {title, author, keyword} = query;
  let results = []
  results = archive.filter(v => {
    // check property if it exists, otherwise true
    if (author) {var author_match = words_match(author, v['author']);}
    else author_match = true;
    if (title) {var title_match = words_match(title, v['title']);}
    else title_match = true;
    if (keyword) {var tags_match = words_match(keyword, JSON.stringify(v));}
    else tags_match = true;
    
    return author_match && title_match && tags_match 
  })
  console.log(results)
  return results
}

module.exports = {
  words_match: words_match,
  results_of_query: results_of_query,
}
