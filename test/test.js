const chaiAsPromised = require("chai-as-promised");
const chai = require("chai");
chai.use(chaiAsPromised);
const should = chai.should()


const pnf = require('../libs/parse_notes_file')
const qar = require('../libs/query_archive')

var dirs = ['./test']

describe('Parsing notes (yaml+markdown) module', () => {

  it('should correctly separate header', () =>{
    // done is used to wait for the promise
    pnf.separate_header_body('test/notes1.txt').should.eventually.eql(
      {
        'header': 'a: 123\nb: 456\ntags: [\'test\']',
        'body': 'dummy dummy\nfoo bar\n\nahahah'
      }
    )
  })

  it('should convert the yaml header into an object', () => {
    converted = pnf.header_to_properties('a: 123\ntags: [john, doe]')
    converted.should.be.a('object')
    converted['a'].should.equal(123)
    converted['tags'].should.be.a('array')
    converted['tags'][0].should.equal('john')
    converted['tags'][1].should.equal('doe')
  })

  it('should convert the body into a list of notes+tags', () => {
    converted = pnf.body_to_notes('this is a line\nand this is another @t{tag1} @t{tag2}\n\nsecond note \n@t{tag3}')
    converted.should.be.a('array')
    converted[0]['tags'].should.be.a('array')
    converted[1]['tags'][0].should.be.equal('tag3')
    converted[0]['text'].should.be.a('string')
    converted[0]['text'].should.be.equal('this is a line\nand this is another')
    converted[1]['text'].should.be.equal('second note')
  })


  it('should create an entry starting from the parsed header and a note', () => {
    entry = pnf.create_entry(
      {author: 'John Doe', tags: ['history', 'future']},
      {text: 'I was born and will live\nsome time', tags: ['personal']}
    )
    entry.should.be.a('object')
    entry['tags'].should.eql(['history', 'future', 'personal'])
  })

  it('should parse a file into a list of notes, using the header to set their properties', () => {
    file_to_entries('test/notes1.txt').should.eventually.eql(
      [
        { a: 123, b: 456, tags: [ 'test' ], refs: [], text: 'dummy dummy\nfoo bar' },
        { a: 123, b: 456, tags: [ 'test' ], refs: [], text: 'ahahah' }
      ]
    )

    file_to_entries('test/notes2.txt').should.eventually.eql(
      [
        { a: 135, b: 246, tags: [ 'test', 'inline tags', 'mytag' ], refs: [], text: 'this is a tagged text' },
        { a: 135, b: 246, label: 'mylabel', tags: [ 'test', 'inline tags', 'mytag' ], refs: [], text: 'this is a labelled paragraph' },
        { a: 135, b: 246, tags: [ 'test', 'inline tags', 'mytag' ], refs: ['mylabel'], text: 'this is a reference to a paragraph' },
      ]
    )
  })

});




describe('Querying archive module', () => {

  it('should recognize two matching strings of words', () => {
    qar.words_match('aaa, bbb', 'ccc').should.equal(false)
    qar.words_match('aaa, bbb', 'bbb').should.equal(true)
    qar.words_match('aaa, bbb', 'bbb, ccc').should.equal(true)
    qar.words_match('aaa, esp, gua.auto', 'rautoms: fff').should.equal(true)
    qar.words_match('Scuotilama', 'Guglielmo Scuotilama').should.equal(true)
  })

  it('should correctly query an archive', () => {
    qar.results_of_query({author: 'Scuotilama'}, 
      [
        {author: 'Scuotilama', title: 'Firenzeo'},
        {author: 'Guglielmo Scuotilama', title: 'Principe Quear'},
        {author: 'Ricevente', title: 'La umana tragedia'}
      ]).should.eql([
        {author: 'Scuotilama', title: 'Firenzeo'},
        {author: 'Guglielmo Scuotilama', title: 'Principe Quear'}])

    qar.results_of_query({keyword: 'firenze'}, 
      [
        {author: 'Scuotilama', title: 'Firenzeo'},
        {author: 'Guglielmo Scuotilama', title: 'Principe Quear'},
        {author: 'Ricevente', title: 'La umana tragedia'}
      ]).should.eql([
        {author: 'Scuotilama', title: 'Firenzeo'}])

    qar.results_of_query({keyword: 'Scuotilama'}, 
      [
        {author: 'Scuotilama', title: 'Firenzeo'},
        {author: 'Guglielmo Scuotilama', title: 'Principe Quear'},
        {author: 'Ricevente', title: 'La umana tragedia'}
      ]).should.eql([
        {author: 'Scuotilama', title: 'Firenzeo'},
        {author: 'Guglielmo Scuotilama', title: 'Principe Quear'}])

    qar.results_of_query({title: 'cloro'}, 
      [
        {author: 'Scuotilama', title: 'Firenzeo'},
        {author: 'Guglielmo Scuotilama', title: 'Principe Quear'},
        {author: 'Ricevente', title: 'La umana tragedia', text: 'Nel mezzo del camino vi era un ceppo'},
        {author: 'Ghepardi', tags: ['triste'], title: 'Troppo cloro in quest\'acqua'},
        {author: 'Ghepardi', tags: ['triste', 'disperato'], title: 'Solo col Cloroformio'}
      ]).should.eql([
        {author: 'Ghepardi', tags: ['triste'], title: 'Troppo cloro in quest\'acqua'},
        {author: 'Ghepardi', tags: ['triste', 'disperato'], title: 'Solo col Cloroformio'}

      ])

  })

});
