const fs = require('fs');
const path = require('path');

const pnf = require('./libs/parse_notes_file');
const qar = require('./libs/query_archive');
//import {render_archive} from './libs/rendering';

const express = require('express');
const app = express();

const jade = require('pug');
const { enrich_archive } = require('./libs/parse_notes_file');
const { arch } = require('os');
app.set('view engine', 'pug')
app.set('views', './views');


let archive = []

var dirs = process.argv.slice(2);

for (let dir of dirs) 
{
  read_dir(dir, archive).then((d) => {console.log(d)})
}


app.get('/', (req, res) => {
  res.render('index', {
    archive: enrich_archive(archive) //rendered_archive.map(entry => `${Object.keys(entry).map(k => ' '+k+': '+entry[k])}`)
    //archive: render_archive(enrich_archive(archive)) //rendered_archive.map(entry => `${Object.keys(entry).map(k => ' '+k+': '+entry[k])}`)
  });
});

app.get('/query', (req, res) => {
  results = qar.results_of_query(req.query, enrich_archive(archive));
  let {title, author, keyword} = req.query;

  res.render('index', {
    title: title,
    author: author,
    keyword: keyword,
    archive: results
  });
});

app.listen(8080, () => {})


